package cs.mad.flashcards

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        print("Flashcard\n---------\n")

        val flashcards = Flashcard.sampleCards()

        for (flashcard in flashcards) {
            print("term: " + flashcard.term + ", definition: " + flashcard.definition + "\n")
        }

        print("FlashcardSet\n-----------\n")

        val flashcardSets = FlashcardSet.sampleCardSets()

        for (flashcardSet in flashcardSets) {
            print("title: " + flashcardSet.title + "\n")
        }
    }
}