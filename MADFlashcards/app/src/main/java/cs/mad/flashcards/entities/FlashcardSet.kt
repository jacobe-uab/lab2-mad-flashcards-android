package cs.mad.flashcards.entities

data class FlashcardSet(val title: String) {
    companion object {
        fun sampleCardSets(): ArrayList<FlashcardSet> {
            val titles = arrayOf("Algebra", "Calculus", "Biology", "Chemistry", "Physics", "English", "Spanish", "French", "US History", "World History")

            var flashcardSets = ArrayList<FlashcardSet>()

            for (title in titles) {
                flashcardSets.add(FlashcardSet(title))
            }

            return flashcardSets
        }
    }

}