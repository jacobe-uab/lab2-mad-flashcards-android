package cs.mad.flashcards.entities

data class Flashcard (val term: String, val definition: String) {
    companion object {
        fun sampleCards(): ArrayList<Flashcard> {
            val terms = arrayOf("cpu", "gpu", "apu", "ram", "hdd", "ssd", "bios", "lan", "wan", "vpn")
            val definitions = arrayOf("central processing unit", "graphics processing unit", "arithmetic processing unit", "random access memory", "hard disk drive", "solid state drive", "basic input/output system", "local area network", "wide area network", "virtual private network")

            var flashcards = ArrayList<Flashcard>()

            for (i in 0..9) {
                flashcards.add(Flashcard(terms[i], definitions[i]))
            }

            return flashcards
        }
    }

}